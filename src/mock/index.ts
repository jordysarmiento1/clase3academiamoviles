const personas = [
    {
        nombre: "Jordy",
        edad: 23
    },
    {
        nombre: "Felipe",
        edad: 24
    },
    {
        nombre: "Carlos",
        edad: 25
    },
    {
        nombre: "Alex",
        edad: 22
    }
]

const usuarios = [
    {
        _id: 1,
        username: 'admin',
        password: '123456'
    },
    {
        _id: 2,
        username: 'admin2',
        password: '123456'
    },
    {
        _id: 3,
        username: 'admin3',
        password: '123456'
    },
    {
        _id: 4,
        username: 'admin4',
        password: '123456'
    }
]

const numeros : number[] = [0, 1, 2, 3]

export default personas;

export {
    personas,
    numeros,
    usuarios
}