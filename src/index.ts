import express from 'express';

const app = express();
import exampleRoute from './routes/example.routes'

app.use(express.json())

const PORT : number = 3000;

const url : string = `http://localhost:${PORT}`

// app.get('/', (req, res) => {
//     res.send({
//         success: 'true',
//         message: 'Servidor ejecutandose correctamente'
//     })
// })

app.use('/example',exampleRoute)

app.listen(PORT, () => {
    console.log(`Escuchando en el puerto ${PORT}, visite la url: ${url}`)
})